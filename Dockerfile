FROM node:16

RUN apt-get update && apt-get install -y chromium

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true
ENV PATH /app/node_modules/.bin:$PATH

WORKDIR /app

COPY ./package.json ./

RUN npm install --production
RUN npm install -g nodemon

COPY . .

CMD ["nodemon", "src/index.js"]
