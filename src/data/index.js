import { Sequelize, Model, DataTypes } from 'sequelize'
import LOGGER from '../utils/logger.js';
import 'dotenv/config'

export const sequelize = new Sequelize(process.env.DATABASE_URL, { logging: false });

export class Event extends Model { }

Event.init({
  artist: DataTypes.STRING,
  price: DataTypes.STRING,
  place: DataTypes.STRING,
  date: DataTypes.STRING
}, { sequelize, modelName: 'events' });

async function createDatabase() {
  try {
    LOGGER.info('Syncing database...');
    await sequelize.sync()
    return
  } catch (error) {
    LOGGER.error(error);
    throw error;
  }
}

export default createDatabase
