import LOGGER from "../../utils/logger.js"
import { Event } from "../index.js"

export async function addEvents(events) {
  const newEvents = []
  try {
    for await (const event of events) {
      const [row, created] = await Event.findOrCreate({
        where: { artist: event.artist, date: event.date },
        defaults: { artist: event.artist, date: event.date, price: event.price, place: event.place }
      })
      if (created) {
        newEvents.push(row.get())
      }
    }
  } catch (error) {
    LOGGER.error(error)
  }
  return newEvents
}

export async function findEvents() {
  try {
    return Event.findAll()
  } catch (error) {
    LOGGER.error(error)
    return []
  }
}