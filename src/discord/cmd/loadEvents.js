import { addEvents } from "../../data/repository/events.js";
import { getEvents } from "../../scraping/getEvents.js";
import LOGGER from "../../utils/logger.js";
import { notify } from "./notify.js";

export async function loadEvents() {
  // const events = [{artist: 'JUNGLE', price: 'ENTRE $ 30.500 Y $ 139.750', place: 'Estadio Bicentenario De La Florida', date: 'JUE 01 DIC 9:00 pm'}]
  const events = await getEvents()
  
  const newEvents = await addEvents(events)
  LOGGER.info('New events found: ', newEvents.length);

  if (newEvents.length > 0) {
    newEvents.forEach(event => notify(event))
  } else {
    LOGGER.info('No new events found');
  }
}