import LOGGER from "../../utils/logger.js"
import { client } from "../index.js"

export async function notify(event) {
  const notified = new Set()
  var message = `:tada: New Events found! :metal: \n${event.artist} - ${event.date} - ${event.place} (${event.price})\n`
  client.channels.cache.forEach(channel => {
    if (channel.type = "GUILD_TEXT") {
      const id = channel.guild.systemChannelId
      if (notified.has(id)) {
        return
      }
      const guildChannel = client.channels.cache.get(id)
      LOGGER.info("Notifying to channel #" + guildChannel.name)
      
      guildChannel.send(message)
      notified.add(id)
    }
  })
}
