import LOGGER from "../../utils/logger.js"
import { client } from "../index.js"

export async function sendMessage(message) {
  const notified = new Set()
  client.channels.cache.forEach(channel => {
    if (channel.type = "GUILD_TEXT") {
      const id = channel.guild.systemChannelId
      if (notified.has(id)) {
        return
      }
      const guildChannel = client.channels.cache.get(id)
      LOGGER.info("Sending message to channel #" + guildChannel.name + '; message: ' + message)
      
      guildChannel.send(message)
      notified.add(id)
    }
  })
}
