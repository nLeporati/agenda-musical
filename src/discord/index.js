import { Client, Intents } from 'discord.js';
import LOGGER from '../utils/logger.js';

export const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES] })

const responses = {
  hola: (message) => {
    const channel = client.channels.cache.get(message.channelId)
    channel.send('Rock & Roll Baby!!!')
  }
}

client.on('messageCreate', async message => {
  const bootId = client.user.id
  
  if (message.author.id === bootId) {
    return
  }
  
  const mentions = message.mentions.users

  if (mentions.size === 1 && mentions.first().id === bootId) {
    const option = message.cleanContent.replace("@​agenda-musical ", "")
    
    const response = responses[option]

    if (response !== undefined) {
      response(message)
    } else {
      await message.reply('No te entiendo, escribe bien aweonao!')
    }
  }
});

async function createDiscordBot() {  
  await client.login(process.env.DISCORD_TOKEN);
  LOGGER.info(`DiscordBot Logged in as ${client.user.tag}`);
}

export default createDiscordBot
