import createTimer from './scheduler/index.js';
import createDatabase from './data/index.js'
import createDiscordBot from './discord/index.js'
import LOGGER from './utils/logger.js';
import 'dotenv/config'

async function main() {
  LOGGER.info("############################");
  LOGGER.info("#### > _FiNtuaL-BoOt_ < ####");
  LOGGER.info("############################");
  await createDatabase()
  await createDiscordBot()
  createTimer()
}

main()
