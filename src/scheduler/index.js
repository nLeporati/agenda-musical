import tasktimer from 'tasktimer'
import LOGGER from '../utils/logger.js';
import { lookAccounts } from './tasks/lookAccounts.js';
import { searchEvents } from './tasks/searchEvents.js';

const tasks = [
  // searchEvents,
  lookAccounts
]

function createTimer() {
  const timer = new tasktimer.TaskTimer(1000); // 1000 * 60 * 60) // 1 hora
  timer.add(tasks)
  timer.start()

  LOGGER.info(`Timer started`);
  
  return timer
}

export default createTimer
