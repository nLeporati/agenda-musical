import { sendMessage } from "../../discord/cmd/sendMessage.js"
import { searchAccounts } from "../../scraping/tasks/searchAccounts.js"
import LOGGER from "../../utils/logger.js"



export const lookAccounts = {
  id: 'look-accounts',
  tickInterval: 12,
  totalRuns: 0,
  callback(task) {
    LOGGER.info(`Running task: ${task.id}`)
    searchAccounts().then(data => {
      if (data.error != null) {
        sendMessage(`**Fintual status updates!** failed :(\n${data.error}`)
      } else {
        let message = `**Fintual status updates! look it >.>**\n\n`
        data.accounts.forEach(account => {
          message += `> ${account.name}\nbalance: ${account.balance}\ndepositaste: ${account.depositaste}\nvariacion: ${account.variacion}\n\n`
        })
        sendMessage(message)
      }
    })
  }
}
