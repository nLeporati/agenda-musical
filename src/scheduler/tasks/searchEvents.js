import { loadEvents } from "../../discord/cmd/loadEvents.js"
import LOGGER from "../../utils/logger.js"

export const searchEvents = {
  id: 'search-events',
  tickInterval: 8,
  totalRuns: 0,
  callback(task) {
    LOGGER.info(`Running ${task.id} task`)
    loadEvents()
  }
}
