import puppeteer from 'puppeteer'
import LOGGER from '../utils/logger.js';

export async function getEvents() {
  try {
    LOGGER.info("Searching for events...");
    const path = process.env.BROWSER_PATH
    const url = 'https://www.agendamusical.cl/lista-conciertos/'
    const browser = await puppeteer.launch({
      executablePath: path,
      args: ['--no-sandbox']
    })
    const page = await browser.newPage()

    await page.goto(url)
    
    let data = await page.evaluate(() => {
      
      const getMonthEvents = (events) => {
        let results = []
        events.forEach(event => {
          const dateSpan = event.querySelector('.evcal_cblock')

          const ems = dateSpan.querySelectorAll('em')
          const date = `${ems[0].innerText} ${ems[1].innerText} ${ems[2].innerText} ${ems[3].innerText}`
      
          const artist = event.querySelector('.evcal_desc2.evcal_event_title')?.innerText
          const price = event.querySelector('.evo_below_title')?.innerText
          const place = event.querySelector('.event_location_name')?.innerText
      
          results.push({ artist, price, place, date })
        })
        return results
      }

      let results = []
      let months = document.querySelectorAll('#evcal_list')

      months.forEach((month) => {
        const events = getMonthEvents(month.querySelectorAll(".evcal_list_a"))
        results.push(...events)
      })

      return results
    })

    await browser.close()

    return data
  } catch (error) {
    LOGGER.error(error)
    return []
  }
}
