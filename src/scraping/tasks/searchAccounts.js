import puppeteer from 'puppeteer-extra'
import LOGGER from '../../utils/logger.js';
import stealthPlugin from 'puppeteer-extra-plugin-stealth'
import fs from 'fs/promises'

async function openBrowser() {
  try {
    puppeteer.use(stealthPlugin())
    const path = process.env.BROWSER_PATH
    const browser = await puppeteer.launch({
      executablePath: path,
      userDataDir: './tmp',
      args: ['--no-sandbox'],
      headless: true,
    })
    const page = await browser.newPage()
    const pages = await browser.pages();
    pages[0].close()
    return { browser, page }
  } catch (error) {
    throw error
  }
}

export async function searchAccounts() {
  const url = 'https://fintual.cl/app/goals'
  const { browser, page } = await openBrowser()
  
  try {
    let cookies = loadCookies()
    await page.goto(url, { waitUntil: 'networkidle2' })

    const needsLogin = await page.$('.fintual-app-button--google')
    LOGGER.info('needs login: ' + String(needsLogin != null))
    
    if (needsLogin) {
      // Fintual login
      await page.waitForSelector('.fintual-app-button--google');
      await page.waitForTimeout(1000);
      await page.click('.fintual-app-button--google');
      await page.waitForTimeout(1000);
  
      // Google email
      await page.waitForSelector('#identifierId')
      await page.type('#identifierId', process.env.GOOGLE_EMAIL);
      await page.waitForTimeout(1000);
      await page.keyboard.press('Enter');
      await page.waitForTimeout(1000);
  
      // Google password
      await page.waitForSelector('input[type="password"]')
      await page.waitForTimeout(1000);
      await page.type('input[type="password"]', process.env.GOOGLE_PASSWORD);
      await page.waitForTimeout(1000);
      await page.keyboard.press('Enter');
    }

    // Fintual Home
    await page.waitForSelector('.goal-index-page__goal-items')
    await page.waitForTimeout(1000)
    if (cookies == null) {
      cookies = await page.cookies()
      await fs.writeFile('./cookies.json', JSON.stringify(cookies, null, 2))
    }
    
    const accounts = []
    accounts.push(await getAccount(page, `${url}/247618293`))
    accounts.push(await getAccount(page, `${url}/1070927250`))

    await browser.close()
    
    return { accounts }
  } catch (error) {
    await browser.close()
    LOGGER.error(error)
    return { error: error }
  }
}

async function getAccount(page, url) {
  LOGGER.info('Getting account info:' + url)
  await page.goto(url, { waitUntil: 'networkidle2' })
  await page.waitForTimeout(1000)
  return await page.evaluate(() => {
    const name = document.querySelectorAll('.fintual-app-heading__title')[0].textContent
    const amounts = document.querySelectorAll('.summary-charts-stat__amount')
    return {
      name,
      balance: amounts[0].textContent,
      depositaste: amounts[1].textContent,
      variacion: amounts[2].textContent
    }
  })
}

async function loadCookies(page) {
  let cookies = null
  try {
    cookies = await fs.readFile('./cookies.json')
    await page.setCookie(...JSON.parse(cookies))
  } catch (error) { }
  LOGGER.info('cookies loaded: ' + String(cookies != null));
  return cookies
}

const mock = { accounts: [
  {
    name: ' Primary',
    balance: '$ 1.200.000',
    depositaste: '$ 1.500.000',
    variacion: '-$ 300.000'
  },
  {
    name: 'Car Backup',
    balance: '$ 550.000',
    depositaste: '$ 500.000',
    variacion: '$ 50.000'
  },
]}