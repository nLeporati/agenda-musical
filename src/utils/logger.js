import chalk from "chalk"

const LOGGER = {
  info(...message) {
    console.info(chalk.yellow("[app][" + new Date().toISOString() + "][inf]: " + message.reduce((p, c) => p + c)))
  },
  error(...message) {
    console.error(chalk.red(`[app][err][${new Date().toISOString()}][err]: ${message.reduce((p, c) => p + c)}`))
  }
}

export default LOGGER
